'use strict';

let gulp = require('gulp');
let env = require('gulp-env');
let lab = require('gulp-lab');
let nodemon = require('gulp-nodemon');
let sourcemaps = require('gulp-sourcemaps');
let sass = require('gulp-sass');
let gutil = require('gulp-util');
let concat = require('gulp-concat');
let runSequence = require('run-sequence');
let del = require('del');

gulp.task('clean-build', (cb) => {
  return del(['./build'], cb);
});

gulp.task('compile-sass', () => {
  return gulp.src('./app/assets/sass/**/*.scss')
  .pipe(sourcemaps.init())
  .pipe(sass().on('error', gutil.log))
  .pipe(gulp.dest('./build/css'))
  .pipe(concat('styles.css'))
  .pipe(sourcemaps.write('.'))
  .pipe(gulp.dest('./public/css'));
});

gulp.task('compile-assets', (cb) => {
  return runSequence('clean-build', ['compile-sass'], cb);
});

gulp.task('build', (cb) => {
  return runSequence('compile-assets', 'clean-build', cb);
});

gulp.task('run-server-specs', () => {
  return gulp.src('./test/server/**/*.js')
    .pipe(lab('-v -l -C'));
});

gulp.task('test', ['run-server-specs']);

gulp.task('develop', ['compile-assets', 'run-server-specs'], () => {
  return nodemon({
    script: 'app/index.js',
    ext: 'js json scss',
    restartable: 'rs',
    verbose: false,
    env: {
      NODE_ENV: 'development'
    },
    watch: ['app/**/*', 'config/**/*', 'test/**/*', 'gulpfile.js', 'package.json']
  })
  .on('restart', () => {
    return runSequence('compile-assets', 'run-server-specs');
  });
});
