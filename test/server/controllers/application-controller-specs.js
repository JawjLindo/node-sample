'use strict';

let lab = exports.lab = require('lab').script();
let expect = require('code').expect;
let cheerio = require('cheerio');

lab.suite('The Application controller', () => {
  let server = null;

  lab.before((done) => {
    if (global.server) {
      server = global.server;
      return done();
    } else {
      return require('../../../app/index')()
        .then((testServer) => {
          global.server = server = testServer;

          return done();
        })
        .catch((err) => { return done(err); });
    }
  });

  lab.suite('data tests', () => {
    lab.test('should return the status JSON', (done) => {
      let options = {
        method: 'GET',
        url: '/api/status'
      };

      return server.inject(options, (response) => {
        expect(response.statusCode).to.equal(200);
        let result = response.result;
        expect(result.status).to.equal('running');

        return done();
      });
    });

    lab.test('should have valid data when retrieving the home page', (done) => {
      let options = {
        method: 'GET',
        url: '/',
        headers: { accept: 'application/json' }
      };

      return server.inject(options, (response) => {
        expect(response.statusCode).to.equal(200);
        let result = response.result;
        expect(result.title).to.equal('Sample Application');
        expect(result.debugRoutes.length).to.be.at.least(1);

        return done();
      });
    });

    lab.test('should have valid data when retrieving the \'about\' page', (done) => {
      let options = {
        method: 'GET',
        url: '/about',
        headers: { accept: 'application/json' }
      };

      return server.inject(options, (response) => {
        expect(response.statusCode).to.equal(200);
        let result = response.result;
        expect(result.title).to.equal('About | Sample Application');
        expect(result.debugRoutes.length).to.be.at.least(1);

        return done();
      });
    });

    lab.test('should have valid data when retrieving the \'contact\' page', (done) => {
      let options = {
        method: 'GET',
        url: '/contact',
        headers: { accept: 'application/json' }
      };

      return server.inject(options, (response) => {
        expect(response.statusCode).to.equal(200);
        let result = response.result;
        expect(result.title).to.equal('Contact | Sample Application');
        expect(result.debugRoutes.length).to.be.at.least(1);

        return done();
      });
    });

    lab.test('should have valid data when retrieving the \'help\' page', (done) => {
      let options = {
        method: 'GET',
        url: '/help',
        headers: { accept: 'application/json' }
      };

      return server.inject(options, (response) => {
        expect(response.statusCode).to.equal(200);
        let result = response.result;
        expect(result.title).to.equal('Help | Sample Application');
        expect(result.debugRoutes.length).to.be.at.least(1);

        return done();
      });
    });
  });

  lab.suite('UI tests', () => {
    lab.test('should correctly render the home page', (done) => {
        let options = {
          method: 'GET',
          url: '/'
        };

        return server.inject(options, (response) => {
          expect(response.statusCode).to.equal(200);
          let result = response.result;
          let $ = cheerio.load(result, {
            normalizeWhitespace: true
          });

          let headerContainer = $('header').children('div');
          expect(headerContainer.find('a').attr('id')).to.equal('logo');

          let headerNavList = headerContainer.find('ul').find('li');
          expect(headerNavList[0].children[0].children[0].data).to.equal('Home');
          expect(headerNavList[1].children[0].children[0].data).to.equal('Help');
          expect($('title')[0].children[0].data).to.equal('Sample Application');

          let bodyContainer = $('#body-container');
          expect(bodyContainer.find('.center')[0]).to.exist();
          expect(bodyContainer.find('h1')[0].children[0].data).to.equal('Welcome to the Node Sample');
          expect(bodyContainer.find('h2')[0].children[0].data).to.equal('Home page for the project');

          let small = $('footer.footer').find('small')[0];
          expect(small.children[0].data.trim()).to.equal('Generated code by');
          expect(small.children[1].children[0].data.trim()).to.equal('Mike Linde');

          let footerNavList = $('footer.footer').find('nav').find('ul').find('li');
          expect(footerNavList[0].children[0].children[0].data).to.equal('About');
          expect(footerNavList[1].children[0].children[0].data).to.equal('Contact');

          return done();
        });
      });

      lab.test('should correctly render the \'about\' page', (done) => {
          let options = {
            method: 'GET',
            url: '/about'
          };

          return server.inject(options, (response) => {
            expect(response.statusCode).to.equal(200);
            let result = response.result;
            let $ = cheerio.load(result, {
              normalizeWhitespace: true
            });

            let headerContainer = $('header').children('div');
            expect(headerContainer.find('a').attr('id')).to.equal('logo');

            let headerNavList = headerContainer.find('ul').find('li');
            expect(headerNavList[0].children[0].children[0].data).to.equal('Home');
            expect(headerNavList[1].children[0].children[0].data).to.equal('Help');
            expect($('title')[0].children[0].data).to.equal('Sample Application');

            let bodyContainer = $('#body-container');
            expect(bodyContainer.find('.center')[0]).to.exist();
            expect(bodyContainer.find('h1')[0].children[0].data).to.equal('About the Sample');

            let footerNavList = $('footer.footer').find('nav').find('ul').find('li');
            expect(footerNavList[0].children[0].children[0].data).to.equal('About');
            expect(footerNavList[1].children[0].children[0].data).to.equal('Contact');

            return done();
          });
        });

        lab.test('should correctly render the \'contact\' page', (done) => {
            let options = {
              method: 'GET',
              url: '/contact'
            };

            return server.inject(options, (response) => {
              expect(response.statusCode).to.equal(200);
              let result = response.result;
              let $ = cheerio.load(result, {
                normalizeWhitespace: true
              });

              let headerContainer = $('header').children('div');
              expect(headerContainer.find('a').attr('id')).to.equal('logo');

              let headerNavList = headerContainer.find('ul').find('li');
              expect(headerNavList[0].children[0].children[0].data).to.equal('Home');
              expect(headerNavList[1].children[0].children[0].data).to.equal('Help');
              expect($('title')[0].children[0].data).to.equal('Sample Application');

              let bodyContainer = $('#body-container');
              expect(bodyContainer.find('.center')[0]).to.exist();
              expect(bodyContainer.find('h1')[0].children[0].data).to.equal('Contact Information');

              let footerNavList = $('footer.footer').find('nav').find('ul').find('li');
              expect(footerNavList[0].children[0].children[0].data).to.equal('About');
              expect(footerNavList[1].children[0].children[0].data).to.equal('Contact');

              return done();
            });
          });

          lab.test('should correctly render the \'help\' page', (done) => {
              let options = {
                method: 'GET',
                url: '/help'
              };

              return server.inject(options, (response) => {
                expect(response.statusCode).to.equal(200);
                let result = response.result;
                let $ = cheerio.load(result, {
                  normalizeWhitespace: true
                });

                let headerContainer = $('header').children('div');
                expect(headerContainer.find('a').attr('id')).to.equal('logo');

                let headerNavList = headerContainer.find('ul').find('li');
                expect(headerNavList[0].children[0].children[0].data).to.equal('Home');
                expect(headerNavList[1].children[0].children[0].data).to.equal('Help');
                expect($('title')[0].children[0].data).to.equal('Sample Application');

                let bodyContainer = $('#body-container');
                expect(bodyContainer.find('.center')[0]).to.exist();
                expect(bodyContainer.find('h1')[0].children[0].data).to.equal('Help');

                let footerNavList = $('footer.footer').find('nav').find('ul').find('li');
                expect(footerNavList[0].children[0].children[0].data).to.equal('About');
                expect(footerNavList[1].children[0].children[0].data).to.equal('Contact');

                return done();
              });
            });
    });
});
