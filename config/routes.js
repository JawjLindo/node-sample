'use strict';

const Joi = require('joi');

let loadHomeRoutes = function(routes) {
    let Controller = require('../app/controllers/application-controller');

    routes.push({
      method: 'GET',
      path: '/{param*}',
      handler: {
        directory: {
          path: 'public'
        }
      }
    });

    routes.push({
      method: 'GET',
      path: '/',
      config: {
        id: 'home.get',
        tags: ['api', 'page'],
        handler: Controller.home,
        description: 'Home page of the application'
      }
    });

    routes.push({
      method: 'GET',
      path: '/api/status',
      config: {
        id: 'api.status',
        handler: Controller.statusApiHandler,
        tags: ['api'],
        description: 'API for the current status of the server',
        response: {
          schema: Joi.object().keys({
            status: Joi.string().required().valid('running', 'stopped')
          })
        }
      }
    });

    routes.push({
      method: 'GET',
      path: '/about',
      config: {
        id: 'about.get',
        tags: ['api', 'page'],
        handler: Controller.about,
        description: 'About page of the application'
      }
    });

    routes.push({
      method: 'GET',
      path: '/contact',
      config: {
        id: 'contact.get',
        tags: ['api', 'page'],
        handler: Controller.contact,
        description: 'Contact information'
      }
    });

    routes.push({
      method: 'GET',
      path: '/help',
      config: {
        id: 'help.get',
        tags: ['api', 'page'],
        handler: Controller.help,
        description: 'Application help'
      }
    });


  };

module.exports = exports = (server) => {
  let routes = [];

  loadHomeRoutes(routes);

  return server.route(routes);
};
