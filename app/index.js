'use strict';

const Hapi = require('hapi');
const config = require('config');
const path = require('path');
const Inert = require('inert');
const Vision = require('vision');
const Pack = require('../package.json');
const HapiSwagger = require('hapi-swagger');

process.env.NODE_ENV = process.env.NODE_ENV || config.get('NODE_ENV');

const server = new Hapi.Server();

const loadGoodPlugin = () => {
  const options = {
    opsInterval: 1000,
    reporters: [
      {
        reporter: require('good-console'),
        events: {
          log: '*',
          response: '*'
        }
      },
      {
        reporter: require('good-file'),
        events: {
          log: '*',
          response: '*'
        },
        config: config.get('logs.info.path')
      },
      {
        reporter: require('good-file'),
        events: {
          error: '*'
        },
        config: config.get('logs.error.path')
      }
    ]
  };

  return new Promise((resolve, reject) => {
    require('mkdirp')(config.get('logs.base-path'), (err) => {
      if (err)
        return reject(err);
      server.register({
        register: require('good'),
        options: options
      }, (err) => {
        if (err) {
          server.log(['error'], { message: 'Could not load \'good\' plugin' });
          return reject(err);
        }

        server.log(['info'], { message: 'Loaded \'good\' plugin' });
        return resolve();
      })
    });
  });
};

const loadInertPlugin = () => {
  return new Promise((resolve, reject) => {
    server.register(Inert, (err) => {
      if (err) {
        server.log(['error'], { message: 'Could not load \'Inert\' plugin' });
        return reject(err);
      }

      server.log(['info'], { message: 'Loaded \'Inert\' plugin' });
      return resolve();
    });
  });
};

const configureViewRendering = () => {
  return new Promise((resolve, reject) => {
    const viewsPath = path.join(__dirname, 'views');

    server.register(require('vision'), (err) => {
      if (err) {
        server.log(['error'], { message: 'Could not configure view rendering' });
        return reject(err);
      }

      server.views({
        engines: { handlebars: require('handlebars').create() },
        path: viewsPath,
        layoutPath: path.join(viewsPath, 'layout'),
        layout: true,
        partialsPath: path.join(viewsPath, 'partials')
      });

      server.log(['info'], { message: 'Configured view rendering' });
      return resolve();
    });
  });
};

const loadRoutes = () => {
  return new Promise((resolve, reject) => {
    try {
      require('../config/routes')(server);
    } catch (err) {
      if (err) {
        server.log(['error'], { message: 'Could not load routes' });
        return reject(err);
      }

      server.log(['info'], { message: 'Routes loaded' });
      return resolve();
    }
  });
};

const loadSwaggerPlugin = () => {
  return new Promise((resolve, reject) => {
    const options = {
      info: {
        title: 'Node Sample API Documentation',
        version : Pack.version
      }
    };

    server.register([
      Inert,
      Vision,
      {
        register: HapiSwagger,
        options: options
      }
    ], (err) => {
      if (err) {
        server.log(['error'], { message: 'Could not load \'Swagger\' plugin' });
        return reject(err);
      }

      server.log(['info'], { message: 'Loaded \'Swagger\' plugin' });
      return resolve();
    });
  });
};

const startServer = () => {
  return new Promise((resolve, reject) => {
    server.start((err) => {
      if (err) {
        server.log(['error'], { message: 'Server could not be started' });
        return reject(err);
      }

      server.log(['info'], { message: `Server started at ${server.info.uri}` });
      return resolve();
    });
  });
};

if (!module.parent) {
  const connectionOptions = {
    host: process.env.IP || config.get('server.host'),
    port: process.env.PORT || config.get('server.port')
  };
  server.connection(connectionOptions);

  loadGoodPlugin()
  .then(() => { loadInertPlugin(); })
  .then(() => { configureViewRendering(); })
  .then(() => { loadSwaggerPlugin(); })
  .then(() => { loadRoutes(); })
  .then(() => { startServer(); })
  .catch((err) => { throw err; });
} else {
  module.exports = () => {
    return new Promise((resolve, reject) => {
      server.connection();
      configureViewRendering()
      .then(() => { loadInertPlugin(); })
      .then(() => { loadRoutes(); })
      .then(() => { return resolve(server); })
      .catch((err) => { return reject(err); });
    });
  };
}
