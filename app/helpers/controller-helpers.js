'use strict';

const getFullPageTitle = (name) => {
  let baseTitle = 'Sample Application';
  if (!name)
    return baseTitle;
  return `${name} | ${baseTitle}`;
};
exports.getFullPageTitle = getFullPageTitle;

const replyBasedOnHeaders = (reply, context, viewName, headers) => {
  if (headers && headers.accept) {
    if (headers.accept.toLowerCase() === 'application/json') {
      return reply(context).type('application/json');
    } else {
      return reply.view(viewName, context);
    }
  } else {
    return reply.view(viewName, context);
  }
};
exports.replyBasedOnHeaders = replyBasedOnHeaders;
