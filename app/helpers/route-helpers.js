'use strict';

const _ = require('lodash');

const getRouteTable = (server) => {
  return new Promise((resolve, reject) => {
    let hapiTable = server.table()[0].table;
    let routeTable = [];

    _.forEach(hapiTable, (routeInTable) => {
      let route = {
        method: routeInTable.method,
        path: routeInTable.path
      };
      if (routeInTable.settings.tags)
        route.tags = routeInTable.settings.tags;
      if (routeInTable.settings.id)
        route.id = routeInTable.settings.id;

      routeTable.push(route);
    });

    routeTable = _.sortBy(routeTable, 'path');

    return resolve(routeTable);
  });
};
exports.getRouteTable = getRouteTable;
