'use strict';

const ControllerHelpers = require('../helpers/controller-helpers');
const RouteHelpers = require('../helpers/route-helpers');

const statusApiHandler = (req, reply) => {
  let context = {
    status: 'running'
  };

  return reply(context).type('application/json');
};
exports.statusApiHandler = statusApiHandler;

const home = (req, reply) => {
  let context = { title: ControllerHelpers.getFullPageTitle() };

  if (process.env.NODE_ENV !== 'production') {
    RouteHelpers.getRouteTable(req.server)
    .then((routes) => {
      context.debugRoutes = routes;
      return ControllerHelpers.replyBasedOnHeaders(reply, context, 'application/home', req.headers);
    });
  } else {
    return ControllerHelpers.replyBasedOnHeaders(reply, context, 'application/home', req.headers);
  }
};
exports.home = home;

const about = (req, reply) => {
  let context = { title: ControllerHelpers.getFullPageTitle('About') };

  if (process.env.NODE_ENV !== 'production') {
    RouteHelpers.getRouteTable(req.server)
    .then((routes) => {
      context.debugRoutes = routes;
      return ControllerHelpers.replyBasedOnHeaders(reply, context, 'application/about', req.headers);
    });
  } else {
    return ControllerHelpers.replyBasedOnHeaders(reply, context, 'application/about', req.headers);
  }
};
exports.about = about;

const contact = (req, reply) => {
  let context = { title: ControllerHelpers.getFullPageTitle('Contact') };

  if (process.env.NODE_ENV !== 'production') {
    RouteHelpers.getRouteTable(req.server)
    .then((routes) => {
      context.debugRoutes = routes;
      return ControllerHelpers.replyBasedOnHeaders(reply, context, 'application/contact', req.headers);
    });
  } else {
    return ControllerHelpers.replyBasedOnHeaders(reply, context, 'application/contact', req.headers);
  }
};
exports.contact = contact;

const help = (req, reply) => {
  let context = { title: ControllerHelpers.getFullPageTitle('Help') };

  if (process.env.NODE_ENV !== 'production') {
    RouteHelpers.getRouteTable(req.server)
    .then((routes) => {
      context.debugRoutes = routes;
      return ControllerHelpers.replyBasedOnHeaders(reply, context, 'application/help', req.headers);
    });
  } else {
    return ControllerHelpers.replyBasedOnHeaders(reply, context, 'application/help', req.headers);
  }
};
exports.help = help;
